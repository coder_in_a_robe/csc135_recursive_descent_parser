I wrote the logic for my recursive descent parser in a newer JavaScript derivative called
TypeScript. TypeScript is actually a super set of JavaScript, so any valid JavaScript is
also valid TypeScript. The main thing that sets TypeScript apart from JavaScript is that,
as the name suggest, TypeScript has a typing system built in. Code is written in a way
that is similar to Java or C, where we declare our variables to be of a certain type, and
the reward for this is compile time type checking, saving us from many of the allusive
bugs that come along with JavaScript. Ultimately TypeScript is compiled into JavaScript,
which becomes the code included in our web application. I chose TypeScript because I'm
already familiar with JavaScript, and I wanted to have a chance to use and learn about
TypeScript. Okay, enough about TypeScript, lets get on to the learning tools.

Well, this was actually my first time using TypeScript, so that was technically a learning
tool for this exercise. Okay, really moving on now! The resources that I used for learning
about the recursive descent parser were the reference book for CSC135, Programming
Languages: Principles and Practice, by Kenneth C. Louden, as well as a very informative
PDF that was provided by Professor Lu, called 6.0 Recursive-Descent Parsing.  I appreciate
concise, effective instructions, and the PDF provided by Professor Lu was just that. It
gave me a great, no nonsense explaination of how to begin with a BNF grammar, and
transform that into an actual implemented parser. All of this instruction was contained
within two pages of large font. Kudos to the author! For the lower level details I heavily
referenced the recursive decent parser implemed in C that is given as an example inside of
the Programming Languages textbook, by Kenneth C. Louden. While my parser accepts a
differnt grammar than the one in the textbook, the overall structure of my parser is
modeled after that in the textbook. Some of the other tools that I used were the Pandoc
document converter tool, which allows us to write webpages in markdown that are compiled
to HTML. Markdown is far less terse than HTML, far more readable, and easier to write
and maintain. I also used NodeJS to download all of the TypeScript tools, did I mention
TypeScript?
