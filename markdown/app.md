---
title: Recursive Descent Parser
header-includes:
    <script src="./js/app.js"></script>
---

This recursive descent parser will tell you whether or not your entered string
is valid based upon the below BNF. Valid strings must be terminated with $.

------------------

```text
EXP    ::= EXP + TERM | EXP - TERM | TERM
TERM   ::= TERM * FACTOR | TERM / FACTOR | FACTOR
FACTOR ::= (EXP) | DIGIT
DIGIT  ::= 0 | 1 | 2 | 3
```

### Input Examples
| Valid Input           | Invalid Input        |
| -------------         | ---------------      |
| 2-1$                  | 2-1                  |
| (1+2)/3$              | (1+2)3$              |
| 3+2\*3$               | +3+2\*3$             |
| ((3-1)/((2\*1)/2)+1)$ | ((3-1)/(2\*1)/2)+1)$ |



<div id="container">
#### `Result`
<div id="indicator">GOOD OR BAD</div>
</div>

<form style="margin-top: 15px;">
  <label for="token">Enter Your String</label>
  <input type="text" id="token" name="user_token">
  <button type="button" id="my-btn">Submit</button>
</form>

------------------

<div id="writeup">
### Writeup on the Tool Chain Used Here ###
</div>
I wrote the logic for my [recursive descent parser](/Users/philthy/Spring_2020/CSC135/Homework/PL_4/App/Logic.html) in a JavaScript derivative called
TypeScript. TypeScript is actually a superset of JavaScript, so any valid JavaScript is
also valid TypeScript. The main thing that sets TypeScript apart from JavaScript is that,
as the name suggest, TypeScript has a typing system built in. Code is written in a way
that is similar to Java or C, where we declare our variables to be of a certain type, and
the reward for this is compile time type checking, saving us from many of the allusive
bugs that come along with JavaScript. Ultimately TypeScript is compiled into JavaScript,
which becomes the code included in our web application. I chose TypeScript because I'm
already familiar with JavaScript, and I wanted to have a chance to use and learn about
TypeScript. Okay, enough about TypeScript, lets get on to the learning tools.

Well, this was actually my first time using TypeScript, so that was technically a
learning tool for this exercise. Okay, really moving on now! The resources that I
used for learning about the recursive descent parser were the reference book for
CSC135, Programming Languages: Principles and Practice, by Kenneth C. Louden, as well
as a [very informative PDF](https://csus.instructure.com/courses/59391/files/5422074?module_item_id=1900419) that was provided by Professor Lu, called 6.0
Recursive-Descent Parsing.  I appreciate concise, effective instructions, and the PDF
provided by Professor Lu was just that. It gave me a great, no nonsense explanation
of how to begin with a BNF grammar, and transform that into an actual implemented
parser. All of this instruction was contained within two pages of large font. Kudos
to the author! For the lower level details I heavily referenced the [recursive decent parser](/Users/philthy/Nextcloud/CSUS/Spring_2020/CSC135/Homework/PL_4/App/Parser_Code.html)
implemented in C that is given as an example inside of the Programming Languages
textbook, by Kenneth C. Louden. While my parser accepts a different grammar than the
one in the textbook, the overall structure of my parser is modeled after that in the
textbook. Some of the other tools that I used were the [Pandoc](https://pandoc.org/) document converter
tool, which allows us to write web pages in markdown that are compiled to HTML.
Markdown is far less terse than HTML, far more readable, and easier to write and
maintain. I also used [NodeJS](https://nodejs.org/en/) to download all of the TypeScript tools, did I mention
[TypeScript](https://www.typescriptlang.org/)? And I can't end this without giving a
shout out to the best, most superpower inducing text editor in the world, [vim](https://neovim.io/).

------------------
<div id="footer">
<p>Brought to you by Phil Fernandez
for CSC135 at Sacramento State University</p>
</div>
